### Theme Name:
 BARAKONI

### Theme URI:
https://gitlab.com/vtxWP/barakoni

### Description:
Theme for creating a WP Theme. I am utilizing Bootstrap to realize this project. I believe this to be a little larger than HTML5, but...  Read the documentatin for more info. - This theme is licensed under the MIT License.

### Version:
0.0.1

### License:
MIT

### License URI:
https://gitlab.com/vtxWP/barakoni/blob/master/LICENSE

### Tags:
one-column, two-columns, right-sidebar, flexible-width, custom-header, custom-menu, editor-style, featured-images, microformats, post-formats, rtl-language-support, sticky-post, translation-ready
